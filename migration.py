import requests
from slugify import slugify
from datetime import datetime

template_rest = u'''
{title}
{underline}


:slug: {slug}
:date: {creation_date}
:tags: {etype}

{content}

'''

template_html = u'''
<html>
    <head>
        <title>{title}</title>
        <meta name="slug" content="{slug}" />
        <meta name="date" content="{creation_date}" />
    </head>
    <body>
{content}
    </body>
</html>

'''

templates = {
    u'text/html': template_html,
    u'text/rest': template_rest
}

suffix_mapping = {
    'rest': 'rst',
    'html': 'html'
}

article_path = 'all_articles/{}/{}-{}.{}'
redirect_string_one = "Redirect /{etype}/{eid} /{year}/{month}/{slug}\n"
redirect_string_two = "Redirect /{eid} /{year}/{month}/{slug}\n"


for etype in ('Card', 'BlogEntry'):
    contents = requests.get('https://www.logilab.fr/{}?vid=ejsonexport'.format(etype)).json()
    for content in contents:
        creation_date = datetime.strptime(content['creation_date'], '%Y/%m/%d %H:%M:%S')
        template = templates[content['content_format']]
        content['underline'] = '#' * len(content['title'])
        content['etype'] = etype
        content['slug'] = slugify(content['title'])
        appendix = content['content_format'].split('/')[1]
        extension = 'rst' if appendix == 'rest' else appendix


        if content['content']:
            content['content'] = content['content'].replace(':rql:', 'rql')

        if content['title']:
            content['slug'] = slugify(content['title'])
        else:
            content['slug'] = content['eid']

        with open(article_path.format(etype.lower(),
                                      content['eid'],
                                      content['slug'],
                                      extension), 'w') as f:
            f.write(template.format(**content))

        if etype == 'Card':
            with open('redirects', "a") as g:
                g.write(redirect_string_one.format(etype='card',
                                                   eid=content['eid'],
                                                   year=creation_date.year,
                                                   month=creation_date.month,
                                                   slug=content['slug']))
                g.write(redirect_string_two.format(eid=content['eid'],
                                                   year=creation_date.year,
                                                   month=creation_date.month,
                                                   slug=content['slug']))
        else:
            with open('redirects', "a") as g:
                g.write(redirect_string_one.format(etype='blogentry',
                                                   eid=content['eid'],
                                                   year=creation_date.year,
                                                   month=creation_date.month,
                                                   slug=content['slug']))
                g.write(redirect_string_two.format(eid=content['eid'],
                                                   year=creation_date.year,
                                                   month=creation_date.month,
                                                   slug=content['slug']))
