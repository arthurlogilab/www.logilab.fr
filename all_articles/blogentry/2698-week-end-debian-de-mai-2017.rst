
Week-end Debian de mai 2017
###########################


:slug: week-end-debian-de-mai-2017
:date: 2017/05/11 11:36:24
:tags: BlogEntry

Logilab a le plaisir de sponsoriser le `week-end Debian des 13 et 14 mai 2017 <https://wiki.debian.org/BSP/2017/05/fr/Paris>`_ qui sera l'occasion de corriger les derniers bugs pour se rapprocher de la sortie de `Debian 9 (Stretch) <https://www.debian.org/releases/stretch/>`_ et d'accueillir de nouveaux contributeurs. Un atelier sera par exemple consacré aux contributions graphiques.

.. image:: https://www.debian.org/logos/openlogo-100.png

