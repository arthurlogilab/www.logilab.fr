
Appel à communication SemWeb.Pro 2017
#####################################


:slug: appel-a-communication-semweb-pro-2017
:date: 2017/05/11 11:53:25
:tags: BlogEntry

L'`appel à communication de SemWeb.Pro 2017 <http://www.semweb.pro/semwebpro-2017.html#aac>`_ vient de sortir. Si vous utilisez les techniques du web sémantique dans un cadre qui ne relève pas de la recherche et du prototype, venez présenter vos réalisations à Paris en novembre 2017 dans le cadre de la conférence SemWeb.Pro, organisée par Logilab avec le soutien de l'INRIA et un comité de programme indépendant.

.. image:: http://semweb.pro/data/semwebpro.png

