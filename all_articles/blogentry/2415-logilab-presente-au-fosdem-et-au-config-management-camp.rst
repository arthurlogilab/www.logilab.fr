
Logilab présente au FOSDEM et au Config Management Camp !
#########################################################


:slug: logilab-presente-au-fosdem-et-au-config-management-camp
:date: 2016/01/19 11:48:04
:tags: BlogEntry

Logilab sera présente au FOSDEM, le rendez-vous incontournable du Logiciel Libre en Europe, pour parler de `Salt <http://saltstack.com/community/>`_ dimanche 31 janvier dans la session `Configuration Management devroom <https://fosdem.org/2016/schedule/track/configuration_management/>`_.

À cette occasion, Arthur Lutz présentera une solution innovante de supervision qui s'appuie sur l'orchestration de `Salt`_ en collectant les données dans graphite et en les exploitant dans grafana.

.. image :: https://www.logilab.fr/file/2423?vid=download
  :width: 60%

Logilab participera ensuite au `Config Management Camp <http://cfgmgmtcamp.eu/>`_ lundi 1er février à Gent, où Arthur présentera à nouveau `Salt`_. N'hésitez pas à regarder le `programme <http://cfgmgmtcamp.eu/schedule/index.html>`_ de cet évènement qui promet d'être très intéressant !

.. image :: https://www.logilab.fr/file/2417?vid=download
  :width: 100%

**Rejoignez-nous sur place !**


Suivez nos actualités sur ce blog ou sur Twitter :

- `@logilab <https://twitter.com/logilab>`_

- `@arthurlutz <https://twitter.com/arthurlutz>`_

- `@douardda <https://twitter.com/douardda>`_

