
SemWeb.Pro 2018 : l'appel à proposition est ouvert !
####################################################


:slug: semweb-pro-2018-l-appel-a-proposition-est-ouvert
:date: 2018/05/14 11:15:51
:tags: BlogEntry

Organisée par `Logilab <https://www.logilab.fr/>`_, avec le soutien de l'INRIA_, `SemWeb.Pro`_ est une journée de conférences dédiée au Web Sémantique, qui réunit chaque année de 100 à 150 personnes depuis la première édition en 2011.

.. _INRIA: https://www.inria.fr/
.. _`SemWeb.Pro`: http://semweb.pro/semwebpro-2018.html

**Participer à SemWeb.Pro c'est l'occasion d'échanger avec les membres de la communauté du Web Sémantique, ainsi qu'avec des sociétés innovantes et des industriels qui mettent en œuvre les nouvelles techniques du Web des données.**

Nous vous invitons à soumettre dès à présent vos propositions de présentation afin de partager votre savoir-faire et votre expérience. Chaque présentation durera 20 minutes (hors Questions-réponses). La langue principale est le français, mais les présentations en anglais sont acceptées.

**Procédure de soumission**

Pour soumettre au comité de programme votre proposition de présentation, veuillez envoyer un courrier électronique à contact@semweb.pro avant le vendredi 15 juin 2018 en précisant les informations suivantes :

- titre,
- description en moins de 400 mots,
- auteur présenté en quelques phrases
- liens éventuels vers des démos, vidéos, applications, etc.

**Critères de sélection**

- l'utilisation effective des standards du Web Sémantique est indispensable,
- nous privilégierons les présentations de projets qui sont déjà en production ou qui concernent de nouveaux domaines d'application les démonstrations et vidéos seront appréciées.


.. image :: https://www.logilab.fr/file/2794/raw



**Call for proposal**

Organized by `Logilab <https://www.logilab.fr/>`_, with the support of INRIA_, `SemWeb.Pro`_ is a one-day conference focused on the Semantic Web, which has been gathering between 100 to 150 persons each year since its first edition in 2011.

**Attending SemWeb.Pro is a unique occasion to discuss with members of the Semantic Web community and with innovative companies and industrials implementors of the Web of Linked Data.**

We invite you to send your proposal to share your experience and know-how. Each talk will last 20 minutes (excluding questions). The main language is French, but English talks are welcome.

**Submission procedure**

To submit your talk proposal to the program committee, please send an email to contact@semweb.pro before Friday June 15th, 2018 including the following information :

- title,
- description in less than 400 words,
- bio of the author in a few sentences,
- links to demos, videos, web applications, etc.

**Selection criteria**

- actual use of some of the Semantic Web standards is mandatory, we favor projects that are already at the production stage or that open new application domains

- demonstrations and videos are valued.

