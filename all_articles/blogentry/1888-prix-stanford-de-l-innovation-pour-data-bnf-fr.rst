
Prix Stanford de l'innovation pour data.bnf.fr
##############################################


:slug: prix-stanford-de-l-innovation-pour-data-bnf-fr
:date: 2013/03/01 16:45:29
:tags: BlogEntry

Logilab a la fierté de réaliser pour le compte de la Bibliothèque nationale de France l'application `data.bnf.fr <http://data.bnf.fr>`_, qui vient de remporter avec Gallica le `prix Stanford 2013 de l'innovation dans le domaine des bibliothèques <http://library.stanford.edu/projects/stanford-prize-innovation-research-libraries-spirl/2013-spirl-winners>`_. Merci à la communauté qui développe `CubicWeb <http://www.cubicweb.org>`_ d'avoir fourni les fondations sur lesquelles nous avons construit data.bnf.fr.

