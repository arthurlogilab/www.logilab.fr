
Salt au Elsass JUG le 29 juillet 2015
#####################################


:slug: salt-au-elsass-jug-le-29-juillet-2015
:date: 2015/07/27 15:51:54
:tags: BlogEntry

Arthur Lutz fera une présentation de son utilisation combinée de `Salt <http://saltstack.org/>`_, `Mercurial <http://mercurial.selenic.com/>`_, `Docker <http://docker.io>`_ et `Python <http://python.org>`_ pour gérer des infrastructures de systèmes informatiques à l'occasion de la prochaine réunion du Elsass JUG qui aura lieu `mercredi 29 juillet 2015 à Strasbourg <http://www.meetup.com/ElsassJUG/events/224144952/>`_.

