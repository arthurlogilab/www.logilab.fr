
Le meetup Python Nantes monitoring c'est ce soir !
##################################################


:slug: le-meetup-python-nantes-monitoring-c-est-ce-soir
:date: 2016/10/05 16:18:16
:tags: BlogEntry

Membre toujours actif de la communauté Python, Logilab soutient le *meetup Python Nantes monitoring* qui aura lieu ce soir, à 19h à la `Cantine <http://cantine.atlantic2.org/>`_\ , située au 11 impasse Juton à Nantes.

.. image :: https://www.logilab.fr/file/2661/raw


À cette occasion, `Arthur Lutz <https://twitter.com/arthurlutz>`_\ , de Logilab, présentera divers outils dédiés au test en python. Au programme :

* introduction aux tests unitaires

* lancer les tests : ``unitest`` de base, ``py.test``, ``nose``, ``pytest``, etc.

* ``tox`` pour lancer les tests dans des ``virtualenv``

* l'intégration continue avec python (``jenkins``, ``travis``, etc.)

* les tests en production : ``healthcheck`` au cœur de l'application 

**Entrée gratuite, mais inscription obligatoire.**

`INSCRIVEZ-VOUS ! <http://www.meetup.com/fr-FR/Nantes-Python-Meetup/events/233581812/>`_

