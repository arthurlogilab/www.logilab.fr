
Battle des plate-formes Open Data - octobre 2014
################################################


:slug: battle-des-plate-formes-open-data-octobre-2014
:date: 2014/09/23 13:14:31
:tags: BlogEntry

Logilab défendra l'honneur de CubicWeb lors de la `Battle des plate-formes Open Data <http://battleopendata.eventbrite.fr/>`_ qui aura lieu au Mans le 3 octobre 2014. Cette démonstration de force s'appuiera sur les `travaux en cours pour la Gironde <http://demo.cubicweb.org/CG33CKAN/>`_.


.. image :: http://www.logilab.fr/file/2207/raw/battleopendata.jpg

`CubicWeb <http://www.cubicweb.org>`_ est un logiciel libre utilisé pour publier des données ouvertes sur le Web, par exemple par la Bibliothèque nationale de France et le département de la Gironde.

