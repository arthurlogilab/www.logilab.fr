
Barcamp OpenScience à Toulouse (fév 2014)
#########################################


:slug: barcamp-openscience-a-toulouse-fev-2014
:date: 2014/02/04 15:20:11
:tags: BlogEntry

Logilab et `Hack your PhD`_ organisent un *barcamp* `Open
Science <https://fr.wikipedia.org/wiki/Science_ouverte>`_
le mardi 25 février 2014 à 18h30 au bar El Deseo,
`11 rue des Lois à Toulouse`__.

__ http://www.openstreetmap.org/#map=16/43.6058/1.4388
.. _`Hack your PhD`: http://hackyourphd.org/2014/02/apero-open-science-a-toulouse/

Le but de cette réunion informelle et gratuite est de favoriser les
échanges entre tous les acteurs intéressés par un aspect de
l'*Open Science* : Open Data, les rapports Sciences & Société,
Open Source, Open Access, Big Data & Data Science, etc.

`Inscrivez-vous <http://framadate.org/studs.php?sondage=mi6p1s9sx681jben>`_ pour faciliter l'organisation et pour toute question, `contactez-nous </contact>`_.

