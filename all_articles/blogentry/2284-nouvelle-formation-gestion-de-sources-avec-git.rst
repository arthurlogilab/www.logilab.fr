
Nouvelle formation "Gestion de sources avec Git"
################################################


:slug: nouvelle-formation-gestion-de-sources-avec-git
:date: 2015/02/23 10:44:05
:tags: BlogEntry

Une nouvelle formation à destination des personnes souhaitant `utiliser Git pour gérer les sources d'un logiciel`_, vient d'être ajoutée à notre catalogue.

Sur le modèle de notre `formation d'introduction à Mercurial`_, des connaissances préalables de la gestion de sources ne sont pas indispensables, mais restent très utiles.

Cette nouvelle formation sera disponible en inter-entreprises comme en intra-entreprise.

.. _`utiliser Git pour gérer les sources d'un logiciel`: https://www.logilab.fr/formations/git-base

.. _`formation d'introduction à Mercurial`: https://www.logilab.fr/formations/hg-base

