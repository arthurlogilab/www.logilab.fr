
Logilab aux Rencontres Régionnales du Logiciel Libre à Nantes
#############################################################


:slug: logilab-aux-rencontres-regionnales-du-logiciel-libre-a-nantes
:date: 2014/09/17 13:00:24
:tags: BlogEntry

En attendant les rencontres `à Toulouse <http://www.logilab.fr/blogentry/2189>`_, nous serons présents aux `Rencontres Régionales du Logiciel Libre à Nantes <http://rrll.alliance-libre.org/>`_ vendredi 19 septembre 2014. 

.. image:: http://www.logilab.fr/file/2200/raw/logo-rrll-2014-nantes.png
  :align: center

Le `Conseil National du Logiciel Libre <http://www.cnll.fr/>`_ a mis en place une tournée annuelle des régions à travers des évènements organisés avec des structures locales. À Nantes il s'agit d'`Alliance Libre <http://alliance-libre.org/>`_ et `Cap Libre <http://www.caplibre.fr>`_. 

Le programme est aussi publié sur `le site de CapLibre <http://rrllsp2013.caplibre.fr/rencontres-regionales-du-logiciel-libre-et-du-secteur-public/programme-nantes-25-novembre-2013>`_, en espérant vous y voir.

