
Nouvelles sessions de formation en juin / juillet 2013
######################################################


:slug: nouvelles-sessions-de-formation-en-juin-juillet-2013
:date: 2013/03/07 12:14:44
:tags: BlogEntry

Trois sessions inter-entreprises supplémentaires sont programmées pour les mois de juin et juillet 2013, en plus des sessions déjà prévues pour le semestre en cours :

* `Créer des paquets debian`_ les 11 et 12 juin 2013

* `Programmation objet avec Python`_ du 17 au 21 juin 2013

* `Développer une application avec CubicWeb`_ du 1er au 5 juillet 2013

Consultez le `programme complet`_.

.. _`Développer une application avec CubicWeb`: http://www.logilab.fr/formations/cubicweb-dev
.. _`Programmation objet avec Python`: http://www.logilab.fr/formations/python-base
.. _`Créer des paquets debian`: http://www.logilab.fr/formations/debian-pkg
.. _`programme complet`: http://www.logilab.fr/formations

