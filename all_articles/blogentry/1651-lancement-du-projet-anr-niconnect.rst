
Lancement du projet ANR Niconnect
#################################


:slug: lancement-du-projet-anr-niconnect
:date: 2012/02/08 11:06:54
:tags: BlogEntry

Le projet Niconnect_ vient d'être accepté_ par l'Agence Nationale de la Recherche. Ce projet, qui regroupe l'INRIA, l'INSERM, le CEA, l'APHP et Logilab, va permettre de généraliser l'usage des techniques d'imagerie cérébrale à des fins de diagnostic et de recherche clinique.

.. _Niconnect: http://media.enseignementsup-recherche.gouv.fr/file/Bio-informatique_2/95/7/NiConnect_206957.pdf
.. _accepté: http://www.enseignementsup-recherche.gouv.fr/cid59286/20-laureats-pour-la-seconde-vague-des-appels-a-projets-de-l-action-sante-et-biotechnologies.html

