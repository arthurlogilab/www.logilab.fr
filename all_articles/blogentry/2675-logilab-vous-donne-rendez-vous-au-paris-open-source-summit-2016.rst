
Logilab vous donne rendez-vous au Paris Open Source Summit 2016
###############################################################


:slug: logilab-vous-donne-rendez-vous-au-paris-open-source-summit-2016
:date: 2016/11/15 16:50:56
:tags: BlogEntry

.. image :: https://www.logilab.fr/file/2676/raw

**Retrouvez-nous au stand C6-D5 du salon**

16 & 17 novembre 2016 Dock Pullman, plaine Saint-Denis

Nous vous accueillerons avec plaisir au salon Paris Open Source Summit pour parler logiciel libre, données ouvertes et Web sémantique.

`Validez votre participation ! <http://www.opensourcesummit.paris/preinscription.html?utm_source=phplist33&utm_medium=email&utm_content=HTML&utm_campaign=Logilab+vous+donne+rendez-vous+au+Paris+Open+Source+Summit+2016>`_

