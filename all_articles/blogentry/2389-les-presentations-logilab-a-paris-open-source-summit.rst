
Les présentations Logilab à Paris Open Source Summit
####################################################


:slug: les-presentations-logilab-a-paris-open-source-summit
:date: 2015/11/24 18:34:08
:tags: BlogEntry

Deux présentations Logilab ont eu lieu à la première édition de Paris `Open Source Summit <http://opensourcesummit.paris/>`_ qui a eu lieu les 18 et 19 novembre aux Docks, à Saint-Denis, aux portes de Paris.

**Système d'Archivage Électronique Mutualisé**

Sylvain Thénault a co-présenté le `projet SAEM : Système d'Archivage Électronique Mutualisé <http://saem.e-bordeaux.org/>`_ en compagnie de Pascal Romain et Pierre-Etienne Cassagnau du `Conseil Départemental de la Gironde <http://www.gironde.fr/jcms/j_6/accueil>`_ dans le cadre des retours d'expérience et solutions des entreprises.

Cette présentation allie le point de vue du client (Conseil Départemental de Gironde) et notre regard technique (Logilab), en particulier sur l'utilisation du logiciel libre `CubicWeb <https://www.cubicweb.org/>`_ et des technologies du `Web Sémantique <https://www.logilab.fr/web-semantique>`_. Vous pouvez la visualiser en `HTML <http://slides.logilab.fr/2015/poss2015_SAEM/>`_.

**Salt pour tester son infrastructure open stack / docker**

David Douard a présenté `Utiliser Salt pour tester son infrastructure sur open stack ou docker <http://slides.logilab.fr/2015/poss2015_salt-docker/#/>`_ dans le cadre de la session "Devops".

`Salt <http://saltstack.com/community/>`_ est un outil de gestion de configuration centralisé généralement utilisé pour configurer et orchestrer son infrastructure système en bénéficiant de la conservation et de l'historisation des fichiers de configuration dans un entrepôt source géré par mercurial ou git.

**Revoir les présentations**

Les présentations sont également accessibles sur `slideshare <http://fr.slideshare.net/logilab/>`_. Elles ont été filmées par les organisateurs du POSS et les vidéos seront bientôt disponibles.

