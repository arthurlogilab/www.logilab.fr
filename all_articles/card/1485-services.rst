
Services
########


:slug: services
:date: 2011/12/01 15:52:37
:tags: Card

Dès sa création, **Logilab** a eu pour politique d'utiliser la technologie non pas comme une fin en soi mais comme un matériau permettant de répondre à des besoins. C'est pourquoi a été créé très rapidement le centre de `compétences </competences>`_  **Logilab** regroupant des experts venus d'horizons différents, à même de réaliser des solutions répondant à des problèmes ardus ou   inhabituels.

S'il agit comme un véritable pont entre le monde de la recherche et ses dernières innovations techniques et le monde de l'industrie, le centre de compétences **Logilab** n'en garde pas moins une approche industrielle  garantissant le respect des coûts et des délais lors de la conception et la réalisation d'un logiciel ou lors de prestations de service.

En s'adossant à ce centre de compétences, **Logilab** est donc à même de   proposer à ses clients des services de grande qualité.

Formation
_________

Les formations sont animées par des experts du sujet et adaptées aux      besoins des participants, sur la base du `catalogue </formations>`_.

Conseil
_______

Les experts de **Logilab** peuvent vous accompagner durant toutes les      étapes de vos projets.

Développement
_____________

Les équipes de **Logilab** peuvent être mises à votre disposition pour      réaliser la conception, le développement ou la maintenance de vos      logiciels (y compris Logiciels Libres).


Intégration
___________

Les experts de **Logilab** sont idéalement placés pour vous aider à      intégrer à votre système d'information les produits de **Logilab** et les      Logiciels Libres qui relèvent de ses domaines de compétences.

L'`équipe commerciale </contact>`_ de **Logilab** est à votre disposition pour vous   apporter toute précision supplémentaire sur les services exposés sur cette    page. En particulier,elle pourra vous aider à formaliser vos besoins et vous proposer une prestation adaptée à ceux-ci.

