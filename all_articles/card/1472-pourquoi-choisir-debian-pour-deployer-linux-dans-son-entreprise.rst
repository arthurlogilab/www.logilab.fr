
Pourquoi choisir Debian pour déployer Linux dans son entreprise ?
#################################################################


:slug: pourquoi-choisir-debian-pour-deployer-linux-dans-son-entreprise
:date: 2011/12/01 15:52:37
:tags: Card

`Debian GNU/Linux <http://www.debian.org/>`_

Qu'est-ce que Debian ?
    
Debian est l'une des quatre principales distributions de Linux. A l'opposé de ses concurrentes que sont RedHat, Mandrake et Suse, elle n'est pas le produit d'une entreprise privée et n'a pas pour unique cible les architectures Intel et le noyau Linux. Debian est le fruit du travail d'une association internationale structurée et bien organisée ayant pour objectif et slogan "Debian : un système d'exploitation universel".
    
A tout seigneur, tout honneur, commençons par l'`introduction <http://www.debian.org/intro/>`_ que le projet Debian propose sur son site et en particulier par les `bonnes raisons de choisir Debian <http://www.debian.org/intro/why_debian.fr.html>`_, ainsi que quelques `questions fréquemment posées <http://www.debian.org/intro/about.fr.html>`_. Cette présentation est bien complétée par un document clair et concis, écrit par un développeur Debian français, intitulé `Debian en milieu professionnel <http://ouaza.com/static/conf/pres-lug68_slideshow.pdf>`_.
    
Reprenons quelques chiffres. Plus de mille développeurs officiels, aidés de milliers de contributeurs, ont empaqueté plus de six mille logiciels qui sont distribués par plus d'une centaine de serveurs miroirs, ce qui représente plus de 80 Go de logiciels compilés pour onze architectures différentes, le tout disponible en près de vingt-huit langues.
    
Il est important de préciser que la distribution Debian comporte trois branches : stable, testing et unstable. La nouvelle version d'un paquet est ajoutée à unstable et n'est transférée dans testing qu'après une dizaine de jours, si aucun problème n'a été signalé. Après plusieurs mois de développements et de tests, la branche testing deviendra la nouvelle version stable de la distribution. L'objectif de la distribution stable n'est pas donc pas d'avoir des logiciels récents mais d'avoir des systèmes sûrs et extrêmement stables qui ne demandent presqu'aucune maintenance, ce qui convient parfaitement pour des serveurs et des équipements réseau.  La branche testing est quant à elle suffisamment stable pour être utilisée au quotidien sans problème et peut donc servir pour des postes de travail ou des serveurs de développement. La branche unstable en revanche n'est recommandée que pour les personnes capables de maintenir elles-mêmes leur machine.
    
Debian a longtemps eu la réputation d'être difficile à installer et à configurer par un non initié. Cette réputation n'est plus aujourd'hui méritée puisque les nouvelles versions du programme d'installation détectent automatiquement le matériel présent sur la machine et ne demandent plus à l'utilisateur d'identifier les pilotes de périphériques nécessaires à son fonctionnement. Au-delà de cette installation initiale, les utilitaires d'installation de logiciels et la qualité de l'intégration des logiciels entre eux donnent toute leur mesure. Ceci a longtemps permis aux utilisateurs de Debian de prétendre en souriant que la mauvaise qualité du programme d'installation n'était pas un problème puisqu'on n'avait à l'utiliser qu'une seule fois dans sa vie et que contrairement à d'autres distributions, toutes les mises à jour, y compris les mises à jour d'une version majeure à la suivante, pouvait se faire en tapant une seule commande : apt-get.
  
Quelles entreprises utilisent Debian ?
    
Le projet Debian maintient une liste des `entités qui utilisent la distribution <http://www.debian.org/users>`_ et souhaitent le faire savoir. Il est difficile d'estimer avec précision quels sont les grands comptes qui ont fait le choix de Debian, puisqu'ils sont peut nombreux à donner des détails de ce type et se limitent le plus souvent à signaler qu'ils utilisent Linux. Nous avons cependant connaissance de quelques exemples : Hewlett Packard, France Telecom (voila.fr), Free Telecom (free.fr), EDF R&D, Selftrade (bourse en ligne), NASA, National Security Agency of USA (Secure Linux), Europcar.
    
Suite au changement de tarification de RedHat, Europcar a décidé de remplacer RedHat par Debian sur l'ensemble de ses postes de travail. Hewlett Packard a standardisé son environnement de recherche et développement Linux sur la base de Debian. 01 Informatique a publié en mars 2004 un article `intitulé Le PC Linux fait une percée dans l'entreprise <http://www.01net.com/Pdf/DMR200402230581032.pdf>`_ qui cite comme distribution principale RedHat, Debian et Suse et comme utilisateurs, Total, le Ministère de l'Intérieur et un éditeur belge. 
  
Popularité
    
Le site DistroWatch, consacré aux distributions de Linux, donne Debian quatrième dans son `classement de popularité de mai 2004 <http://www.distrowatch.com/stats.php?section=popularity>`_, derrière Mandrake, Fedora/RedHat et Knoppix. Knoppix étant une variante de Debian, on peut donc considérer que Debian occupe deux des quatre premières places :-).
    
Le magazine allemand Linux Enterprise nomme Debian `meilleure distribution pour l'entreprise <http://www.linuxenterprise.de/itr/service/show.php3?id=104&nodeid=35>`_ devant Suse et RedHat. Un autre magazine allemand, Linux Magazin, nomme  `Debian devant Knoppix (!) et Suse <http://www.linux-magazin.de/Artikel/ausgabe/2003/12/award/award.html>`_.
    
D'après un sondage Netcraft, les gens qui, depuis son changement de tarification, `abandonnent RedHat passent à Debian <http://www.serverwatch.com/news/article.php/3306161>`_, ce qui en fait la `deuxième distribution la plus utilisée <http://news.netcraft.com/archives/2004/01/28/debian_fastest_growing_linux_distribution.html>`_ au monde, derrière RedHat.
    
Le Monde Informatique quant à lui, en 2003, dit de Debian qu'elle est la distribution la plus complète et la plus stable.
  
Support commercial
    
Le site de Debian propose une liste, nécessairement incomplète, de  `personnes et sociétés <http://www.debian.org/consultants/#France>`_ qui offrent un support commercial. La société `Logilab <http://www.logilab.fr/>`_, auteur de cet article, offre une aide au déploiement, à l'administration et des formations spécifiques à Debian.
    
Parmi les grands de l'informatique, Hewlett Packard s'est fait remarquer par son `offre de support commercial <http://www.hp.com/hpinfo/newsroom/press/2006/060814a.html>`_ de Debian. HP propose à ses clients de livrer ses machines avec `RedHat, Suse ou Debian installé <http://www.01net.com/article/233442.html>`_ et annonce avoir renforcé son support pour Debian à la demande de ses clients, qui sont de plus en plus nombreux à souhaiter disposer de Debian sur leurs postes de travail et serveurs.
  
Déployer Debian
    
Debian offre des avantages pour un déploiement à grande échelle. En particulier, l'outil de gestion des paquets dpkg permet d'obtenir la liste des paquets installés (dpkg --get-selections), mais aussi de fixer la liste des paquets à installer (dpkg --set-selections). Là où d'autres systèmes d'exploitation mettent en place des mécanismes compliqués pour répliquer une machine maître sur des machines esclaves, il suffit à un administrateur Debian de fixer la liste des paquets et de décompresser une archive de /etc pour qu'une machine soit identique à une autre. La qualité des paquets Debian assure au passage que tous les fichiers de configuration des logiciels installés sont bien dans /etc, contrairement à d'autres distributions qui n'ont pas toujours le même souci du détail.
    
Une autre solution pour un déploiement à grande échelle consiste à utiliser des clients légers, qui démarrent par le réseau grâce au protocoles PXE et DHCP et ne nécessitent pas de disques durs. Les données et les sauvegardes sont ainsi centralisées sur des serveurs et mieux protégées. De plus, les installations sont simplifiées puisqu'il suffit d'ajouter un logiciel sur le serveur pour qu'il soit immédiatement disponible sur tous les clients. Les postes clients sont plus faciles à maintenir et moins coûteux car ils ne comportent pas de logiciel spécifique ni de disque dur. Si un poste client a un problème matériel, il peut être remplacé immédiatement sans que cela ait la moindre conséquence pour l'utilisateur. Logilab a écrit en juin 2000 l'un des premiers `guide <http://www.logilab.org/projects/netboot-howto>`_ décrivant l'installation d'un parc de ce type et utilise cette solution en interne depuis cette date. D'autres sociétés ont adopté cette approche et en font la `publicité <http://www.01net.com/article/231729.html>`_.    
    
On a vu ci-dessus que Debian avait pour avantage de proposer trois branches, qui couvraient des besoins différents. Il est évident que cette possibilité de maintenir plusieurs branches en parallèle est une qualité de la distribution, qu'une entreprise peut exploiter à son avantage. Lors d'un déploiement industriel, on a toujours à faire face à des conflits de versions entre les dépendances des outils développés en interne ou par des prestataires et les éléments de la plate-forme choisie. Pour prendre un exemple concret, on peut citer le cas d'un client de Logilab qui souhaitait distribuer un logiciel pour RedHat 7, 8 et 9 et qui s'est vu contraint d'en modifier les sources et de le compiler plusieurs fois car RedHat 7.2 inclut la version 2.95 de gcc, mais RedHat 8 inclut gcc 3. De même tel logiciel fonctionnera avec Debian testing qui inclut Gnome 2 mais pas avec Debian stable qui n'inclut que Gnome 1. Ces problèmes sont courants et ont motivé le créateur du projet Debian à créer une société, Progeny, dont le métier est de proposer à ses clients de maintenir des plate-forme Linux sur la base de la distribution Debian.

Logilab, au fait de la nécessité d'introduire une part de sur-mesure dans le déploiement d'une distribution Debian, propose à ses clients de les aider à adapter et maintenir leur propre variante de Debian. Ceci n'est viable que parce que cette distribution est suffisamment modulaire pour le permettre, mais aussi parce que plusieurs sociétés dans le monde ont ouvert la voie et travaillent à modulariser Debian. 
  
Des distributions Debian sur mesure !
    
`Componentized Linux <http://platform.progeny.com/componentized-linux/>`_ est un projet visant à bâtir, sur la base de Debian, des modules interdépendants et versionnés séparément. Cette modularisation réduit l'influence des interdépendances entre paquets en les synthétisant par des dépendances entre modules. Mettre au point une plate-forme Linux revient alors à faire un choix de versions de modules et y rajouter les paquets manquants. Une telle adaptation est beaucoup plus difficile à faire avec une distribution classique.

`Custom Debian Distribution <http://wiki.debian.net/index.cgi?CustomDebian>`_ est un outil développé par le projet Debian pour faciliter la création de méta-paquets et donc de distributions spécialisées telles que `Debian Junior <http://www.debian.org/devel/debian-jr/>`_ (pour les moins de 14 ans), `Debian Med <http://www.debian.org/devel/debian-med/>`_ (pour les professions médicales), `Demudi <http://www.demudi.org/>`_ (pour les professionnels du son et du multimédia) ou encore `SkoleLinux <http://www.skolelinux.org/>`_ et Debian-Edu (pour les écoles et universités). Pour plus de détails sur ces distributions spécialisées, on pourra se reporter à un `présentation en anglais <http://2007.rmll.info/article224.html>`_.
    
`Morphix <http://www.morphix.org/>`_ est une distribution `modulaire <http://am.xs4all.nl/phpwiki/index.php/ModuleListing>`_ fondée sur Debian, qui ne nécessite aucune installation sur disque dur et démarre à partir d'un simple cédérom. `Knoppix <http://www.knoppix.org/>`_ est aussi une distribution "LiveCD" fondée sur Debian, qui connaît depuis plusieurs mois une croissance fulgurante, car elle présente l'avantage de pouvoir être essayée sur tout ordinateur, sans installation et sans remise en cause du système déjà installé.

