
Florent Cayré
#############


:slug: florent-cayre
:date: 2014/06/26 15:02:15
:tags: Card

Vous pouvez me contacter par : 

* courriel : `florent.cayre@logilab.fr <mailto:florent.cayre@logilab.fr>`_
* messagerie instantanée Jabber/XMPP : `fcayre@jabber.logilab.org <xmpp:fcayre@jabber.logilab.org>`_ et `salon public@conference.jabber.logilab.org <xmpp:public@conference.jabber.logilab.org?join>`_
* téléphone :  +33 1 45 32 03 12

Vous pouvez également consulter mon profil professionnel sur :

* `LinkedIn <http://www.linkedin.com/in/cayre>`_

Prochainement, vous pourrez télécharger mes coordonnées au format VCard.

