
Nous suivre
###########


:slug: nous-suivre
:date: 2011/12/01 15:54:44
:tags: Card

Blogs (RSS)
===========

.. image :: http://cdn1.iconfinder.com/data/icons/Stickers_clay/128/ccink_rss.png
  :align: right

Il est possible de recevoir des mises à jours de nos publications en utilisant les flux RSS : 

* `logilab.fr <http://feeds.feedburner.com/logilab>`_
* `logilab.org <http://feeds.feedburner.com/logilaborg>`_
* `logilab.org (francais uniquement) <http://feeds.feedburner.com/logilaborg_fr>`_
* `logilab.org (english only) <http://feeds.feedburner.com/logilaborg_en>`_
* `cubicweb.org <http://feeds.feedburner.com/cubicweborg>`_
* `blog.simulagora.com <http://blog.simulagora.com/feeds/all.atom.xml>`_

Ces flux sont fusionnés et lisibles sur la `planète Logilab <http://planet.logilab.fr/>`_.

Microblog (Twitter)
================================

Il est possible de nous suivre sur Twitter et Identi.ca: 

.. image :: http://cdn1.iconfinder.com/data/icons/Stickers_clay/128/ccink_twitter.png
  :align: right

* `@logilab <https://twitter.com/logilab>`_
* `@logilab_org <https://twitter.com/logilab_org>`_ 
* `@cubicweb <https://twitter.com/cubicweb>`_ 
* `@semwebpro <https://twitter.com/semwebpro>`_

Liste des comptes twitter des employés de Logilab : https://twitter.com/logilab/employees


Réseaux sociaux
===============

.. image :: http://cdn1.iconfinder.com/data/icons/Stickers_clay/128/ccink_linkedin.png
  :align: right

* LinkedIn__

__ http://www.linkedin.com/company/logilab

* Viadeo__

__ http://fr.viadeo.com/fr/search/rcl/fr/Logilab/fr/

* `BitBucket <https://bitbucket.org/logilab>`_

* `GitHub <https://github.com/logilab?tab=members>`_

* `SlideShare <http://www.slideshare.net/logilab>`_

* `YouTube <https://www.youtube.com/channel/UCZ5aNOda_Zn7QMAVHE3xUJg>`_

* `Vimeo <http://vimeo.com/logilab>`_

* `Dailymotion <http://www.dailymotion.com/group/logilab>`_

Jabber (XMPP) / IRC
=====================

Nous sommes joignables sur jabber (XMPP) `public@conference.jabber.logilab.org <xmpp:public@conference.jabber.logilab.org?join>`_ et IRC (`#pylint sur irc.freenode.net <http://webchat.freenode.net?randomnick=1&channels=pylint>`_)

