
Offres d'emplois et stages
##########################


:slug: offres-d-emplois-et-stages
:date: 2011/12/01 15:52:37
:tags: Card

.. image :: https://www.logilab.fr/file/2622/raw

Poursuivant son développement en 2017, Logilab cherche à pourvoir les postes suivants.

CDI
===

Nous sommes actuellement à la recherche d'ingénieurs pour renforcer nos équipes de R&D.

    * `CDI - Directeur de projets agiles </ddp1>`_
    * `CDI - Développement web (client / *Front-end*) </inge3>`_
    * `CDI - Développement informatique et web sémantique </inge2>`_

Stages
======

Nous proposons également des stages longs, pour des étudiants au niveau Bac+4/5.

..
  Ergonomie logicielle
  -----------------------

  * `Ergonomie des interfaces dans les applications CubicWeb </stage-cw-ergo>`_

Web sémantique et bases de données
--------------------------------------

* `Mise en place d'une architecture REST dans la plateforme CubicWeb </stage-cw-rest>`_
* `Utilisation de cadres d'application Javascript dans la plateforme CubicWeb </stage-cw-js>`_
* `Intégration de données externes dans la plateforme CubicWeb </stage-cw-integr>`_
* `Utilisation de SPARQL dans la plateforme CubicWeb </stage-cw-sparql>`_

Informatique scientifique et simulation numérique
-------------------------------------------------

* `Jupyter dans Simulagora (plateforme de simulation numérique dans le cloud) </Jupyter-dans-Simulagora>`_


..
  * `Amélioration des fonctionnalités collaboratives et gestion des fichiers dans Simulagora </stage-simulagora-collab>`_
  * `Amélioration ergonomique de  Simulagora, une plateforme de simulation numérique dans les nuages </stage-simulagora>`_
  * `Visualisation 3D dans une plateforme de simulation numérique </stage-web3d-sci>`_
  * `Analyse d'incertitudes d'une structure mécanique avec Simulagora </stage-simulagora-etude-mecanique>`_

Administration système et nuages de serveurs (Cloud computing)
--------------------------------------------------------------



..
  * `Test-Driven System Administration </stage-test-driven-sysadmin>`_
  * `Convergence des outils de communication SIP/XMPP </stage-outils-com>`_
  * `Ergonomie Application Web </stage-ergo-appli-web>`_


Envoyer une candidature
==========================

Envoyez votre candidature (CV + lettre de motivation, format PDF ou HTML) par courrier électronique à personnel@logilab.fr. Veuillez noter que nous n'acceptons que les documents dans les formats ouverts suivants : pdf, odt, html, texte, ps.

