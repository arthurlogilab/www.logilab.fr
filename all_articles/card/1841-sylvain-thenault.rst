
Sylvain Thénault
################


:slug: sylvain-thenault
:date: 2012/10/02 11:23:40
:tags: Card

Contacts : 

* courriel : `sylvain.thenault@logilab.fr <mailto:sylvain.thenault@logilab.fr>`_
* messagerie instantanée jabber/xmpp : `syt@jabber.logilab.org <xmpp:syt@jabber.logilab.org>`_ et `salon public@conference.jabber.logilab.org <xmpp:public@conference.jabber.logilab.org?join>`_
* téléphone :  05 62 17 16 42 ou 01 45 32 03 12
* twitter : `@sythenault <http://twitter.com/sythenault>`_
* `github <https://github.com/sthenault>`_
* `bitbucket <https://bitbucket.org/sthenault>`_
* foaf :  `logilab.org <http://www.logilab.org/cwuser/sthenault?vid=foaf>`_, `cubicweb.org <http://www.cubicweb.org/cwuser/sthenault?vid=foaf>`_

.. * identi.ca : `@douardda (identi.ca) <http://identi.ca/douardda>`_
.. `identi.ca <https://identi.ca/douardda/foaf>`_

Télécharger le contact en format vcard (à venir)

