
CDI - DÉVELOPPEMENT INFORMATIQUE ET WEB SEMANTIQUE
##################################################


:slug: cdi-developpement-informatique-et-web-semantique
:date: 2011/12/01 15:52:37
:tags: Card

Vous êtes passionné d'informatique et souhaitez comprendre et maîtriser le fonctionnement du système d'exploitation, du réseau, savoir architecturer et concevoir une application et la développer avec  agilité. 

Quand vous entendez Python, vous ne pensez pas immédiatement au serpent. Quand on vous dit Java, vous répliquez "trop verbeux" plutôt que "je ne sais pas danser". Quand on mentionne C++, vous pensez "il y a des maux qui sont nécessaires". Quand on vous parle de Fortran, vous vous dites qu'il vous survivra. 
Quand on vous invite à discuter Haskell, vous ne vous préparez pas à manger indien. Quand on vous donne rendez-vous à la Scala, vous ne réservez pas de billet d'avion pour l'Italie. Quand vous récitez l'alphabet, vous placez L(isp) après A et B, mais avant F et C. S'il ne doit en rester que deux, ce seront emacs et vi. 

Vous savez formuler votre CV et votre lettre de motivation en une page et n'envoyez jamais des documents Word à des inconnus.

Rejoignez notre équipe !

Mission
-------

Intégré(e) à l'équipe de développement en charge de la réalisation de projets informatiques ayant trait au web sémantique, vous utiliserez principalement Linux et Python, et vous ferez appel à vos connaissances en matière de conception logicielle et d'architecture de systèmes d'information.

Profil
------

H/F - BAC+5 ou supérieur.

Expérience
----------
Débutant(e)s accepté(e)s.

Compétences / Connaissances souhaitées
--------------------------------------

* Programmation en Environnement Unix/Linux (Python).

* Publication Web : HTML(5) et CSS(3), HTTP(S) et REST, Javascript (jQuery, d3.js, three.js), etc.

* Web sémantique : RDF, OWL, SPARQL, etc.

* Utilisation d'un gestionnaire de version distribué: hg, git.

* Modélisation de données et conception logicielle (UML).

* Méthodes agiles d'organisation (XP, SCRUM, etc.)

* Architecture de système d'information (Bases de données, client-serveur, réseau, objets distribués, etc.).

* Autonomie et capacité à travailler en équipe.

* Anglais.

* Habitude du logiciel libre.

Durée
------

Contrat à Durée Indéterminée.

Lieu
----
Paris (75013) ou Toulouse.

Rémunération
-------------
Rémunération selon le profil.

Candidatures
-------------
Envoyez votre candidature (CV + lettre de motivation, format PDF ou HTML) par courrier électronique à personnel@logilab.fr.

