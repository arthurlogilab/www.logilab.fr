
Mentions Légales
################


:slug: mentions-legales
:date: 2011/12/13 10:20:52
:tags: Card

Les informations des sites Logilab sont fournies telles quelles sans garanties d'aucune sorte, ni explicites ni tacites. En particulier, aucune information apparaissant sur le présent site ne saurait être considérée comme une offre de services ou de produits engageant Logilab de quelque manière que ce soit. Toute personne désireuse de se procurer un des services ou produits présentés ici est priée de contacter Logilab afin de s'informer de la disponibilité du dit produit ou service et des conditions contractuelles qui lui sont applicables.

Tous les produits cités sur ce site sont des marques déposées de leur société respective.


* Copyright 2001-2012 par Logilab S.A.

Éditeur
-------

LOGILAB - S.A. au capital de 100 000 Euros - APE: 6062A - SIREN: 432 746 196 - RCS de Paris - Siège social: 104, boulevard Louis-Auguste Blanqui 75013 PARIS


Hébergeur
---------

LOGILAB - S.A. au capital de 100 000 Euros - APE: 6062A - SIREN: 432 746 196 - RCS de Paris - Siège social: 104, boulevard Louis-Auguste Blanqui 75013 PARIS

