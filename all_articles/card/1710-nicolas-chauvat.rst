
Nicolas Chauvat
###############


:slug: nicolas-chauvat
:date: 2012/06/14 20:38:59
:tags: Card

Contacts : 

* courriel : `nicolas.chauvat@logilab.fr <mailto:nicolas.chauvat@logilab.fr>`_
* téléphone : +33 (0)1 45 32 03 12
* twitter : `@nchauvat <http://twitter.com/nchauvat>`_
* identi.ca : `@nchauvat (identi.ca) <http://identi.ca/nchauvat>`_
* `LinkedIn <http://fr.linkedin.com/in/nicolaschauvat>`_
* `github <https://github.com/nchauvat>`_
* `bitbucket <https://bitbucket.org/nchauvat>`_
* foaf :  `identi.ca <https://identi.ca/nchauvat/foaf>`_, `logilab.org <http://www.logilab.org/cwuser/nchauvat?vid=foaf>`_, `cubicweb.org <http://www.cubicweb.org/cwuser/nchauvat?vid=foaf>`_, `semweb.pro <http://www.semweb.pro/cwuser/nchauvat?vid=foaf>`_

Télécharger le contact en format vcard (à venir)

