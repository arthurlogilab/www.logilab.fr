
David Douard
############


:slug: david-douard
:date: 2012/07/05 14:50:16
:tags: Card

Contacts : 

* courriel : `david.douard@logilab.fr <mailto:david.douard@logilab.fr>`_
* messagerie instantanée jabber/xmpp : `david@jabber.logilab.org <xmpp:david@jabber.logilab.org>`_ et `salon public@conference.jabber.logilab.org <xmpp:public@conference.jabber.logilab.org?join>`_
* téléphone :   01 83 64 25 26 ou 01 45 32 03 12
* twitter : `@douardda <http://twitter.com/douardda>`_
* identi.ca : `@douardda (identi.ca) <http://identi.ca/douardda>`_
* `github <https://github.com/douardda>`_
* `bitbucket <https://bitbucket.org/douardda>`_
* foaf :  `identi.ca <https://identi.ca/douardda/foaf>`_, `logilab.org <http://www.logilab.org/cwuser/ddouard?vid=foaf>`_, `cubicweb.org <http://www.cubicweb.org/cwuser/ddouard?vid=foaf>`_, `semweb.pro <http://www.semweb.pro/cwuser/ddouard?vid=foaf>`_

Télécharger le contact en format vcard (à venir)

