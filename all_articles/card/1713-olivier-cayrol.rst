
Olivier Cayrol
##############


:slug: olivier-cayrol
:date: 2012/06/18 14:53:12
:tags: Card

Vous pouvez me contacter par : 

* courriel : `Olivier.Cayrol@logilab.fr <mailto:Olivier.Cayrol@logilab.fr>`_
* messagerie instantanée Jabber/XMPP : `ocy@jabber.logilab.org <xmpp:ocy@jabber.logilab.org>`_ et `salon public@conference.jabber.logilab.org <xmpp:public@conference.jabber.logilab.org?join>`_
* téléphone :  +33 1 45 32 03 12

Vous pouvez également consulter mon profil professionnel sur :

* `LinkedIn <http://www.linkedin.com/in/oliviercayrol>`_
* `Viadeo <http://fr.viadeo.com/fr/profile/olivier.cayrol>`_

Prochainement, vous pourrez télécharger mes coordonnées au format VCard.

