
UML et le Processus Unifié chez Logilab
#######################################


:slug: uml-et-le-processus-unifie-chez-logilab
:date: 2011/12/01 15:52:37
:tags: Card

Dans toutes ses activités, **Logilab** s'appuie sur UML (*Unified Modeling Language*), le standard de description des systèmes logiciels.

Selon les besoins, **Logilab** utilise diverses méthodes de développement logiciel. Pour la réalisation, sa préférence va aux `méthodes agiles </outils-methodes-agiles>`_ qui offrent le meilleur rendement. Cependant, elle utilise toujours le processus unifié (UP ou Unified Process), en particulier pour les étapes de spécification et d'analyse.

