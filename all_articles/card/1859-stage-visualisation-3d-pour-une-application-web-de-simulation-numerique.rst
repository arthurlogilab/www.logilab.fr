
Stage - Visualisation 3D pour une application Web de simulation numérique
#########################################################################


:slug: stage-visualisation-3d-pour-une-application-web-de-simulation-numerique
:date: 2012/11/12 17:39:35
:tags: Card

Contexte
========

L'évolution actuelle de l'informatique montre une migration de la
majorité des applications du client lourd vers le client Web. Cette
tendance a de nombreux avantages, notamment en termes de maintenance
et d'installation puisque les utilisateurs n'ont plus qu'à avoir une
navigateur Web récent pour pouvoir accéder à l'application, laquelle
est plus facilement maintenue puisqu'elle est uniquement installée sur
le serveur.

Les applications scientifiques nécessitent généralement de pouvoir
afficher et même modifier des éléments en 3 dimensions : géométries,
maillages, champs de vecteurs, etc. Or, il existe aujourd'hui des
techniques et des bibliothèques permettant de travailler en 3D dans un
navigateur Web, notamment WebGL_, et `three.js`_.

De premiers travaux, en 2013, ont permis de valider la faisabilité de
la visualisation dans le navigateur de données scientifiques et de
maillages issus d'exemples industriels, avec la fluidité nécessaire à
une utilisation réelle.

.. _WebGL: http://www.khronos.org/webgl/
.. _`three.js`: http://threejs.org/

Problématique
=============

Logilab développe une plateforme de simulation numérique mettant à la
disposition de scientifiques un pilote et un gestionnaire de cas de
calcul. Les divers solveurs et outils nécessaires ayant été
préinstallés et configurés, l'utilisateur n'a plus qu'à téléverser ses
données, lancer ses calculs puis télécharger les résultats. La
plateforme de simulation numérique est développée en Python_ en
s'appuyant sur le cadriciel CubicWeb_.

.. _Python: http://www.python.org/
.. _CubicWeb: http://www.cubicweb.org/

L'objectif des travaux proposés ici est de mettre en œuvre, dans la
plateforme de simulation, une fonctionnalité de visualisation des
géométries, des maillages ou des résultats de calcul directement dans
le navigateur, sans avoir besoin de télécharger l'intégralité des
données.

Rôle
====

Intégré à l'équipe de recherche et développement "Simulation
Numérique" de Logilab, sous la tutelle d'un ingénieur spécialiste de
la plateforme CubicWeb, vous l'assisterez dans son travail quotidien
et pourrez être amené à effectuer tout ou partie des travaux suivants,
en collaboration avec l'équipe :

* intégrer, dans la plateforme de simulation, la visualisation dans le
  navigateur d'objets scientifiques 3D,

* mettre en œuvre des techniques permettant de limiter les données à
  transmettre entre le serveur et le client,

* mettre en place sur la plateforme une simulation numérique complète
  illustrant les fonctionnalités développées.

Compétences attendues
=====================

* Bonnes connaissances pratiques en programmation objet,
* Connaissance de l'architecture et des techniques du Web (HTML,
  Javascript, etc.),
* Connaissance du domaine Web 3D (WebGL_, `three.js`_, etc.),
* Connaissances générales en simulation numérique,
* Anglais technique,
* La connaissance du langage Python_ sera appréciée.

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

