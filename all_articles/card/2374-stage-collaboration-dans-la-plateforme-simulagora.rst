
Stage - Collaboration dans la plateforme Simulagora
###################################################


:slug: stage-collaboration-dans-la-plateforme-simulagora
:date: 2015/10/30 17:18:19
:tags: Card

Amélioration des fonctionnalités collaboratives et gestion des fichiers dans Simulagora_ (plateforme de simulagion numérique).


Contexte
========


L'arrivée des nuages de serveurs (*cloud computing*) a remis fortement
en cause la façon d'aborder l'informatique et plus particulièrement le
calcul scientifique. En effet, aujourd'hui, de grandes structures,
comme Amazon ou Google aux États-Unis, mettent à disposition sur
Internet un réseau de serveurs pouvant proposer une puissance de
calcul phénoménale. Ces serveurs sont loués à l'utilisation à un prix
dérisoire, ce qui permet à n'importe qui d'avoir accès à une puissance
de calcul quasiment illimitée. Dans le domaine scientifique, cela
signifie que des simulations qui, hier, semblaient inaccessibles sont
désormais possibles. Toutefois, le processus permettant d'effectuer
des simulations dans un nuage de serveurs est loin d'être simple, et
ses nombreuses opérations peuvent rapidement dérouter les néophytes
peu versés dans l'informatique système et l'administration de
serveurs.


Problématique
=============


Partant de ce constat, Logilab développe une plateforme de simulation numérique mettant à la disposition de scientifiques un pilote et un gestionnaire de cas de calcul. Les divers solveurs et outils nécessaires ayant été préinstallés et configurés, l'utilisateur n'a plus qu'à téléverser ses données, lancer ses calculs puis télécharger les résultats. Il peut échanger données de calculs et programmes avec d'autres utilisateurs, ce qui constitue le coeur des fonctionnalités de collaboration.
L'objectif des travaux proposés ici est d'améliorer ces fonctionnalités collaboratives en repensant le processus de collaboration et en adaptant l'ergonomie de l'application en conséquence.


Rôle
====


Intégré à l'équipe de recherche et développement "Simulation
Numérique" de Logilab, sous la tutelle d'un ingénieur spécialiste de
la mise en œuvre de codes de calcul et de la plateforme CubicWeb_, vous
l'assisterez dans son travail quotidien et pourrez être amené à
effectuer tout ou partie des travaux suivants, en collaboration avec
l'équipe :

* contribuer au développement de la plateforme de simulation numérique
  dans le cloud, Simulagora_


Compétences attendues
=====================

- bonnes connaissances pratiques de la programmation avec le langage Python_
- connaissances en modélisation des données
- Connaissance de l'architecture et des techniques du Web (HTML, Javascript, etc.)
- des connaissances en virtualisation de serveur seront appréciées.

.. _Python: http://www.python.org/
.. _CubicWeb: http://www.cubicweb.org/
.. _Simulagora: http://simulagora.com/


Niveau
======


Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)


Durée
=====


6 mois


Rémunération
============


Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.


Candidatures
============


Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

