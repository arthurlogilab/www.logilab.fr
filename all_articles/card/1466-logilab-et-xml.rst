
Logilab et XML
##############


:slug: logilab-et-xml
:date: 2011/12/01 15:52:37
:tags: Card

Depuis sa création, **Logilab** utilise XML et les standards dérivés pour représenter et manipuler des données. Elle est à l'origine de nombreux outils en Python pour XML et a participé au développement du module XML de la bibliothèque standard de ce langage.

Aujourd'hui, **Logilab** continue à suivre de près ces standards, en particulier dans les domaines de la gestion documentaire, la gestion de connaissances et le Web sémantique.

