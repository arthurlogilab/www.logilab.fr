import os
import requests
from cwclientlib import cwproxy_for

image_url = os.environ.get('IMAGE_URL', 'https://www.logilab.fr/file/{eid}/raw/{data_name}')
image_path = os.environ.get('IMAGE_PATH', './images/{data_name}')
client = cwproxy_for('https://www.logilab.fr/')

query = ('Any F, N, U '
         'where F is File, '
         'F data_name N, '
         'F cwuri U, '
         'F data_format in ("image/jpeg", "image/png")')

resp = client.rql(query)

images = resp.json()
print("Number of images found: {n}".format(n=len(images)))
print("---")

if resp.status_code == 200:
    for (eid, data_name, cwuri) in images:
        if (eid and data_name):
            download_url = image_url.format(eid=eid, data_name=data_name)
            print("The cwuri is: {}".format(cwuri))
            print ("The image link is: {}".format(download_url))
            print ('---')

            response = requests.get(download_url, stream=True)
            if response.status_code == 200:
                with open(image_path.format(data_name=data_name), 'wb') as f:
                    for chunk in response.iter_content(1024):
                        f.write(chunk)
            else:
                print("Error: {status} on url {url}".format(status=r.status_code,
                                                            url=download_url))
        else:
            print('Something went wrong with eid {eid}; file {data_name}'.format(eid=eid,
                                                                                 data_name=data_name))
